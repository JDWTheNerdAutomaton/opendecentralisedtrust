# OpenDecentralisedTrust

This project suggests and implements a rating system that protects rated entity data, data originator entity data and incentivises any type of entity to participate in the system.

# Entities participating in an open decentralised rating system

- Rated Entity (Alice): A entity that wants to prove its reliability by being letting its data get rated by an open decentralised trust algorithm. 
- Data Originator Entity (Bob): A second entity that interacts with the rated entity and can provide data points that influence the reliability rating of the rated entity
- Open Rating algorithm developer (Carla): A Group of software developers and data scientists that develops rating algorithms for the data. They define the types of data to be provided, develop the system, and algorithms that are run to give ratings. 
- Rating Requestor (Dave): An entity with a desire to run an open rating algorithm on Alices Data, usually in order to decide if they want to interact with Bob in a certain way.
- The public (Edgar): anyone interacting with the system.  The intents each member of the public are for their own gain. Not that damaging the reputation of any other entity in the system might be a positive incentive for Edgar. The system should be hardened against misuse by Edgar.


# Properties of a open decentralised rating system

- Nobody but the rated entity Alice knows Alices the total sum of data. Each Data Originator Entities Bob only knows the parts that they created for Alices dataset.
- Only knowing Alices or Bobs identification address and publicly available data, it is not possible to find out, who Alice or Bob interacts with.
- The rating is eventually trustworthy: All rating data points submitted by Bob about Alice are verified to be taken into account when running the rating algorithm. 
- Alice cannot manipulate entries, and has to eventually accept negative entries in order to keep the most possible positive rating

# Assumptions about the system environment

- Alice, Bob, Carla and Dave each got their own identification address and private and public keys.

# Incentives for participation

- Alice: wants their data to be protected and their rating to be traceable. Wants to interact with others and prove her trustworthyness. She wants to sell output of algorithm runs on her data on the open market protecting her private information
- Bob: wants to interact with others by providing their product. Wants to make sure that they make a profit by using the system and sending their data over to Alice.
- Carla: Wants to develop a rating service that is not based on storing personal data, but surrounding services. Wants make a profit from the system being used. 
- Dave: Needs the output of a rating algorithm about Alice for some reason. Maybe wants to be sure about trusting Alice. Bob and Dave might be the same entity.

# Interaction between Alice and Bob

Given: 
- Alice has a private pr_a and public key pu_a, Bob has a private pr_b and public pu_b key
- Alice and Bob want to establish an interaction out of which Bob can testify about Alices trustworthyness.

Algorithm:
Ramp up: Initialze the connection channel
- Alice and Bob share their public keys pu_a and pu_b with each other.
- Out of their own private key and the others public key, they each generate a new private and public key (alice: pr_a>b pu_a>b, bob: pr_b>a pu_b>a).
- They share their new public key pu_a>b and pu_b>a with each other.
- They send gas money to these addresses (in a privacy ensuring way. Open: How can this be done to preserve the privacy of the connection between pu_a and pu_a>b and pu_b and pu_b>a respectively? Some intermediate to decouple the keys might be needed, but they then know the link...)
- They deploy the open decentrailized trust contract to the blockchain with their roles assigned to the public keys pu_a>b and pu_b>a
- Alice and Bob privately note that they have a new contract deployed. After a while they each collect their new contracts into a message, and sign that message hash and commit the message to their main address.


Feedback: Bob wants to testify something about Alice
- Bob writes a message in the open rating algorithm format and calculates the hash. 
- Bob signs the hash
- Bob sends the message to Alice. Open: How can he reach Alice?
- Bob sends the signed hash to the open decentralised trust contract, increasing the number of send testates from n to n+1, and storing the hash on the blockchain

If Alice agrees to the message:
- Alice accepts the message and also signs the message hash
- Alice sends the signed has to the open decentralised trust contract, increasing the number of accepted testates from m to m+1.

If Alice does not agree to the message:
- Alice notifies Bob about the disagreement. 
- Bob agrees to the reversion of the message and interacts with the open decentralised trust contract by nullifying the previously send hash.

There are four states the testate can be in: Sent, Nullified, Disagreed, Agreed.

# Interaction between Alice and Dave

- Dave buys a token from Alice to run Carlas Algorithm on Alices Data
- With Ocean protocol, Carlas Algorithm is run on Alices data
- Dave gets the output of Carlas algorithm

# Alices Data

Alices data consists of:
- A set of addresses she owns and has testifiable interactions with Bob. The list is signed and the signature is commited to the blockchain.
- Each address is signed by Bob and Alice
- Each address has a assigned contract address where the message counter between Bob and Alice is stored.
- A number of messages which is equal to the message counter on the blockchain contract

# Carlas algorithm

- It verifies the address list as given with Alices main address hash.
- For each contract address: it verifies the committed messages is equal to the number of messages for that address in Alices data.
- It calculates the score based on whatever makes sense returns the score. 

some notes on carlas algorithm: It might consider only the agreed messages, it might consider nullified, disagreed or sent messages. It might consider problems with verification, and problems with data that has not been signed for a long time by Alice. Whatever makes sense to perform the rating. As a guess, nullified, disagreed or sent messages should probably simply not be taken into account for a rating. The number of interactions or disagreements might be interesting however.


# open questions:

- How to ensure privacy of key pairs (pu_a, pu_a>b) and (pu_b, pu_b>a)? On vanilla blockchain transactions somebody has to send money to the addresses pu_b>a and pu_a>b. If Alice or Bob do that directly, they would be linked, and it would be public information who they are interacting with, redering the complex system useless or overengineered. The gas money could be send from any other address, such as fiat gateways or other third parties with offchain processes attached to it (trusted entities) or other addresses in control of Alice pu1_a and Bob pu2_b, not linked to pu_a and pu_b. For that to work Alice and Bob can never use the same adress for multiple contracts, in order not to spill information. 
- How to incentivize Bob to participate? Bob could also sell his data on open sea, with algorithms tailored for what he can provide. Ideally the data he provides could give insight in the trustworthyness of Alice type users.


